package PartTwo.q24;

import PartTwo.q24.Area;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int C = 2;
        int L = 2;
        Area[][] parkingLot = new Area[C][L];

        Scanner input = new Scanner(System.in);
        String color;

        //Get data for the areas in the parkingLot
        for (int i = 0; i < parkingLot.length; i++) {
            for (int j = 0; j < parkingLot[i].length; j++) {
                System.out.println("--Enter area color: ");
                color = input.next();
                System.out.println("--Enter area capacity: ");
                parkingLot[i][j] = new Area(color, input.nextInt());
            }
        }

        System.out.print("[???] What do you what to do(e/O/I): ");
        char operation = input.next().charAt(0);

        int level; //For the out operation
        int sum;
        boolean active = true; //To break from the loop

        while (active) {

            if (operation == 'e') {
                active = false;
            } else if (operation == 'I') {
                boolean found = false;
                for (int i = 0; i < parkingLot.length & !found; i++) {
                    for (int j = 0; j < parkingLot[i].length & !found; j++) {
                        if (parkingLot[i][j].freeParkingLots()) {
                            System.out.printf("--- Found ---\n[+]level: %d\n[+] area color: %s\n", i, parkingLot[i][j].getColor());
                            found = true;
                        }
                    }
                }

                if (!found) {
                    System.out.println("[ERR] Error couldn't find free parking slots.");
                }

            } else if (operation == 'O') {
                boolean found = false;

                System.out.println("--Enter area color: ");
                color = input.next();

                System.out.println("--Enter level: ");
                level = input.nextInt();

                for (int i = 0; i < parkingLot[level].length & !found; i++) {
                    if (parkingLot[level][i].getColor().equals(color)) {
                        parkingLot[level][i].removeCar();
                        System.out.println("[INFO] Car removed");
                        found = true;
                    }
                }

            }

            System.out.println("---- Free places ----");
            sum = 0;
            level = 0;
            for (Area[] floor : parkingLot) {
                for (Area area : floor) {
                    sum += (area.getAreaCapacity() - area.getCurrentUsed());
                }
                System.out.printf("[+] Level %d: %d\n", level++, sum);
            }

            System.out.print("[???] What do you what to do(e/O/I): ");
            operation = input.next().charAt(0);
        }

    }
}
