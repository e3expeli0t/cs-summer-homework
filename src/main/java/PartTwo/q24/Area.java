package PartTwo.q24;

public class Area {
    private String color;
    private final int areaCapacity;
    private int currentUsed;

    public Area(String color, int areaCapacity) {
        this.areaCapacity = areaCapacity;
        this.color = color;
        this.currentUsed = 0;
    }

    /**
     * Add car to the area
     */
    public void addCar() {
        if (areaCapacity-(currentUsed +1) >= 0) {
            ++this.currentUsed;
        }
    }

    /**
     * remove car from the area
     */
    public void removeCar() {
        if (currentUsed -1 >= 0) {
            --currentUsed;
        }
    }

    /**
     * check for free parking
     * @return boolean
     */
    public boolean freeParkingLots() {
        return !(areaCapacity- currentUsed == 0);
    }

    public int getAreaCapacity() {
        return areaCapacity;
    }

    public int getCurrentUsed() {
        return currentUsed;
    }

    public String getColor() {
        return color;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return "Area{" +
                "color='" + color + '\'' +
                ", areaCapacity=" + areaCapacity +
                ", currentUsed=" + currentUsed +
                '}';
    }
}
