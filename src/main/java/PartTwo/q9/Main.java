package PartTwo.q9;

public class Main {
    public static void main(String[] args) {
        Carriage[] cars = new Carriage[90];
        cars[0]= new Carriage(123, 90);
        Train train = new Train(cars.length, cars, new Engine(22, 7890));

        train.swapEngine(new Engine(67, -9));
        train.addCarriage(new Carriage(153, 90));
        System.out.println(train.numOfTravelers());
        System.out.println(train.avgCarriageTrav());
    }
}
