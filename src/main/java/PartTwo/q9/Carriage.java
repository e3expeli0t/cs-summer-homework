package PartTwo.q9;

public class Carriage {
    private int serial;
    private int numOfTravelers;

    public Carriage(int serial, int numOfTravelers) {

        this.serial = serial;
        this.numOfTravelers = numOfTravelers;
    }

    public int getSerial() {
        return serial;
    }

    public int getNumOfTravelers() {
        return this. numOfTravelers;
    }
}
