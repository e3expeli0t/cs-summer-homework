package PartTwo.q9;

public class Train {
    private int N;
    private Carriage[] Carriages;
    private Engine engine;

    public Train(int n, Carriage[] carriages, Engine engine) {

        N = n;
        Carriages = carriages;
        this.engine = engine;
    }

    public Carriage[] getCarriages() {
        return Carriages;
    }

    public Engine getEngine() {
        return engine;
    }

    public int numOfTravelers() {
        int cnt = 0;
        for (int i = 0; i <  avlIndex(); i++) {
                cnt += this.Carriages[i].getNumOfTravelers();
        }

        return cnt;
    }

    public double avgCarriageTrav() {
        return (double) numOfTravelers()/avlIndex();
    }

    public void swapEngine(Engine engine) {
        this.engine = engine;
    }

    public void addCarriage(Carriage carriage) {
        int as = avlIndex();

        if (as > 0) {
            this.Carriages[as] = carriage;
        }
    }

    private int avlIndex() {
        for (int i = 0; i < this.Carriages.length; i++) {
            if(this.Carriages[i] == null) {
                return i;
            }
        }

        return 0;
    }
}
