package PartTwo.q9;

public class Engine {
    private int LicenceNumber;
    private int yearOfManufactor;

    public Engine(int licenceNumber, int yearOfManufactor) {

        LicenceNumber = licenceNumber;
        this.yearOfManufactor = yearOfManufactor;
    }

    public int getLicenceNumber() {
        return LicenceNumber;
    }

    public int getYearOfManufactor() {
        return yearOfManufactor;
    }
}
