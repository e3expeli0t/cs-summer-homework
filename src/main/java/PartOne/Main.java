package PartOne;

import java.util.Random;

public class Main {

    static Car[] genCars(int startYear, String[]colors, String[] manufactures,String[]models,  int size) {
        Random randomizer = new Random();
        Car[] cars = new Car[size];

        for (int i = 0 ; i < size; i++) {
            String manufacturer = manufactures[randomizer.nextInt(manufactures.length)];
            String color = colors[randomizer.nextInt(colors.length)];
            String model = models[randomizer.nextInt(models.length)];
            cars[i] = new Car(manufacturer, color, model, randomizer.nextInt(999), randomizer.nextInt(50)+startYear);
        }

        return cars;
    }

    public static void main(String[] args) {
        String[]colors = {"blue", "red"};
        String[] manufactures = {"mazda", "coroma", "tesla"};
        String[]models = {"e1", "e2"};
        CompanyCars yosef = new CompanyCars(genCars(2000, models, manufactures, colors,  8), "Y&Y");
        CompanyCars shmulic = new CompanyCars(genCars(1950, models, manufactures, colors,  18), "S&S");

        if (yosef.avgManufactureYear() == Math.max(yosef.avgManufactureYear(), shmulic.avgManufactureYear())) {
            System.out.println(yosef.getName());
        }else {
            System.out.println(shmulic.getName());
        }

    }

}
