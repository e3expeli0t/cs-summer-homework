package PartOne;

public class Car {
    private String manufacturer;
    private String model;
    private String color;
    private int licenseNumber;
    private int yearOfManufacture;

    public Car(String manufacturer, String model,String color,  int licenseNumber, int yearOfManufacture) {
        this.manufacturer = manufacturer;
        this.color = color;
        this.model = model;
        this.licenseNumber = licenseNumber;
        this.yearOfManufacture = yearOfManufacture;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public String getModel() {
        return model;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber(int licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    public int getYearOfManufacture() {
        return yearOfManufacture;
    }
}
