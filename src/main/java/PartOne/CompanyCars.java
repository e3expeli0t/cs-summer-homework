package PartOne;

/**
 * @author e3expeli0t
 */
public class CompanyCars {
    private int numberOfCars;
    private Car[] cars;
    private String name;

    public CompanyCars(Car[] cars, String name) {
        this.numberOfCars = cars.length;
        this.cars = cars;
        this.name = name;
    }

    public int sumByColor(String color) {
        int counter = 0;
        for (Car car : cars) {
            if (car.getColor().equals(color)) {
                counter++;
            }
        }

        return counter;
    }

    public double avgManufactureYear() {
        int yearSum = 0;
        for (Car car : cars) {
            yearSum += car.getYearOfManufacture();
        }

        return (double) yearSum / cars.length;
    }

    /**
     * A manufacturer is dominant if the number of cars that
     * he manufacture is bigger then the avg manufactured cars
     */
    public void dominantManufacturer() {
        double avg = this.CalculateAvg();
        String[] mans = new String[this.numberOfCars];
        for (Car car : this.getCars()) {
            if (!exists(mans, car.getManufacturer()) && this.countOccurrences(car.getManufacturer()) > avg) {
                System.out.println(car.getManufacturer());
            }
        }
    }

    private double CalculateAvg() {
        int nMans = 0;
        String[] mans = new String[this.numberOfCars];
        for (Car car : this.getCars()) {
            if (!exists(mans, car.getManufacturer())) {
                mans[nMans++] = car.getManufacturer();
            }
        }

        return (double)this.numberOfCars/nMans;
    }

    private boolean exists(String[] arr, String elem) {
        for (String s : arr) {
            if (s != null && s.equals(elem)) {
               return true;
            }
        }

        return false;
    }


    private int countOccurrences(String manufacturer) {
        int counter = 0;
        for (Car car : cars) {
            if (car.getManufacturer().equals(manufacturer)) {
                counter++;
            }
        }

        return counter;
    }

    public int getNumberOfCars() {
        return numberOfCars;
    }

    public String getName() {
        return name;
    }

    public Car[] getCars() {
        return cars;
    }
}
