import java.util.Scanner;

public class Area
{	
	String color;
	int NumParking;
	int NumParkingFull;
		
	public Area(String color, int NumParking, int NumParkingFull)
	{
		this.color = color;
		this.NumParking = NumParking;
		this.NumParkingFull = NumParkingFull;
	}
	
	public String Getcolor(){
		return this.color;
	}
	
	public int getNumParking(){
		return this.NumParking;
	}
	
	public int getNumParkingFull(){
		return this.NumParkingFull;
	}
	
	public void setColor(String C){
		this.color = C;
	}
	
	public void setNumParking(int Np){
		this.NumParking = Np;
	}
	
	public void setNumParkingFull(int Npf){
		this.NumParkingFull = Npf;
	}
	
	public String string(){
		return "the color is "+ this.color+" number of parking lots "+ this.NumParking+" number of parking lots full "+ this.NumParkingFull;
	}
	
	public void AddCar(int numCarsToAdd){
		if(numCarsToAdd + this.NumParking <= this.NumParkingFull) {
			this.NumParking += numCarsToAdd;
		}
	}
	
	public void EraseCar(int NumCarsErase){
		this.NumParking = this.NumParking-NumCarsErase;
	}
	
	public boolean isNotFull(){
		return !(this.NumParking == this.NumParkingFull);
	}
}
