import java.util.*;

public class garage
{
	public static Scanner reader= new Scanner (System.in);
	
	static Area setArea() {
		System.out.print("enter color");
		String color = reader.next();
		System.out.println("enter number parking");
		int numParking = reader.nextInt();
		System.out.println("enter number of parking spots");
		int numParkingFull = reader.nextInt();
		return new Area(color , numParking , numParkingFull);
	}
	

	public static void main(String[] args)	
	{
		int L = 2;
		int C = 2;
		
		Area[][] parkingLot = new Area[L][C];
		for(int i = 0; i < L; i++){
			for(int d = 0; d < C; d++){
				parkingLot [i][d] = setArea();
			}	
		}
		
		char code = reader.next().charAt(0);
		boolean freePlace = true, found = false;
		
		while(true){
			if (code == 'I' & freePlace){
				for(int i = 0; i < parkingLot.length && !found; i++) {
					for(int j = 0; j < parkingLot[i].length && !found; j++) {
						if (parkingLot[i][j].isNotFull()) {
							System.out.println("Free spot on level: "+i+" area: "+j+" Area Color:"+parkingLot[i][j].Getcolor());
							parkingLot[i][j].AddCar(1);
							found = true;
						}
					}
					System.out.println("There is no free parking spot");
					freePlace = false;
				}

			} else if (code == 'O') {
				String color = reader.next();
				int floor = reader.nextInt();
				for (int i = 0; i < parkingLot[floor].length; i ++) {
					if (parkingLot[floor][i].Getcolor().equals(color)) {
						parkingLot[floor][i].EraseCar(1);
					}
				}
			}
			code = reader.next().charAt(0);
		}
		int sum = 0;
		for(int i = 0; i < parkingLot.length && !found; i++) {
			for(int j = 0; j < parkingLot[i].length && !found; j++){
				sum += parkingLot[i][j].getNumParkingFull()-parkingLot[i][j].getNumParking();
			}
	}
}